import Vue from 'vue'
import './plugins/axios'
import '@/assets/css/base.css'
import '@/assets/css/style.css'
import App from './App.vue'
import router from './router'
import store from './store'
import util from '@/util/'
import './plugins/vant.js'
import 'vant/lib/index.css';
import {
    Storage
} from "@/util/storage.js"
import VueWechatTitle from 'vue-wechat-title';
Vue.use(VueWechatTitle)
Vue.config.productionTip = false
Vue.use(util)
router.beforeEach((to, from, next) => {
    let jhs = new Storage()
    if (to.name == 'showVideo') {
        next()
        return
    }
    console.log(from);
    if (jhs.getItem('userInfo')) {
        next()
    } else {
        if (to.name != 'loader') {
            router.push({
                path: '/loader'
            })
        }
        next()
    }


})
new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')
