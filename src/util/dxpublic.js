import router from '@/router'
var urls = "https://duxinggj.com/"
var wx = require('weixin-js-sdk');
import axios from "axios"
import {
    Storage
} from './storage.js'
class dxpublic {
    hf(url, cu) {
        router.push({
            path: url,
            query: cu
        })
    }
    /*无浏览记录跳转*/
    hfp(url, cu) {
        router.replace({
            path: url,
            query: cu
        })
    }
    back() {
        router.back(-1)
    }

    dxpost(url, dtat) {
        return new Promise((resolve) => {
            axios({
                method: 'post',
                url: urls + url,
                data: dtat
            }).then((res) => {
                resolve(res.data);
            });
        })
    }
    dxget(url, data) {
        return new Promise((resolve) => {
            axios.get(urls + url, {
                params: data,
            }).then(function (response) {
                if (response.data.code != 0) {
                    return
                }
                resolve(response.data.data);
            })
        })
    }
    dateWeek() {
        var myDate = new Date(); //获取今天日期
        myDate.setDate(myDate.getDate() + 7);
        var dateArray = [];
        var dateTemp;
        var flag = 1;
        var weekday = ["周日", "周一", "周二", "周三", "周四", "周五", "周六"];
        for (var i = 0; i <= 7; i++) {
            let jhba = (myDate.getMonth() + 1),
                jhbb = myDate.getDate()
            dateTemp = (jhba < 10 ? 0 + '' + jhba : jhba) + "-" + (jhbb < 10 ? 0 + '' + jhbb : jhbb);
            myDate.setDate(myDate.getDate() - flag);
            let kjhhs = {}
            kjhhs.name = weekday[myDate.getDay() + 1] || '周日'
            kjhhs.riqi = dateTemp
            dateArray.push(kjhhs);
        }
        let jjhse = dateArray.reverse()
        let jhhhsser = []
        jjhse.map((a, b) => {
            if (b == 0) {
                a.name = '今天'
            }
            if (b == 1) {
                a.name = '明天'
            }
            /*    if (b == 2) {
                    a.name = '后天'
                }*/
            if (b <= 2) {
                jhhhsser.push(a)
            }
        })
        return jhhhsser
    }
    dateWeekdan(t) {
        let myDate = new Date(t); //获取今天日期
        var dateTime = '';
        var dateTemp;
        var weekday = ["周日", "周一", "周二", "周三", "周四", "周五", "周六"];
        let jhba = (myDate.getMonth() + 1),
            jhbb = myDate.getDate()
        dateTemp = (jhba < 10 ? 0 + '' + jhba : jhba) + "-" + (jhbb < 10 ? 0 + '' + jhbb : jhbb);
        let kjhhs = {}
        kjhhs.name = weekday[myDate.getDay()] || '周日'
        kjhhs.riqi = dateTemp
        dateTime = kjhhs
        return dateTime

    }
    // 过滤重复数组
    unique(arr, name) {
        const res = new Map();
        return arr.filter(
            (a) => !res.has(a[name]) && res.set(a[name], 1)
        )
    }
    // 删除多个数据
    deleArray(arra, arrb) {
        arra.map(b => {
            for (let i = arrb.length - 1; i >= 0; i--) {
                if (arrb[i].id === b) {
                    arrb.splice(i, 1);
                }
            }
        })
    }



    // json对象转字符串
    jsonToString(data) {
        let jjhse = ''
        try {
            var _result = [];
            for (var key in data) {
                var value = data[key];
                if (value.constructor == Array) {
                    value.forEach(function () {
                        _result.push(key + '=' + value);
                    });
                } else {
                    _result.push(key + '=' + value);
                }
                jjhse = _result.join('&');
            }
        } catch (e) {
            console.log(e);
        }

        return jjhse
    }
    // 验证ip
    sValidIP(ip) {
        var reg = /^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$/
        return reg.test(ip);
    }
    // 验证手机号
    phone(a) {
        var b = !1;
        return a.match(/^13[0-9]{9}|15[0-9]{9}|14[0-9]{9}|19[0-9]{9}|16[0-9]{9}|17[0-9]{9}|18[0-9]{9}$/) && 11 == a.length && (b = !0), b
    }
    /*验证身份证*/
    car_t(a) {
        var b = !1;
        return /^(^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$)|(^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])((\d{4})|\d{3}[Xx])$)$/.test(a) && (b = !0), b
    }

    wx_config(data) {
        wx.config({
            debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
            appId: data.appId, // 必填，公众号的唯一标识
            timestamp: data.timestamp, // 必填，生成签名的时间戳
            nonceStr: data.nonceStr, // 必填，生成签名的随机串
            signature: data.signature, // 必填，签名，见附录1
            jsApiList: [
                        'checkJsApi',
                        'updateAppMessageShareData',
                        'updateTimelineShareData',
                        'onMenuShareTimeline',
                        'onMenuShareAppMessage',
                        'onMenuShareQQ',
                        'onMenuShareWeibo',
                        'onMenuShareQZone',
                        'hideMenuItems',
                        'showMenuItems',
                        'hideAllNonBaseMenuItem',
                        'showAllNonBaseMenuItem',
                        'translateVoice',
                        'startRecord',
                        'stopRecord',
                        'onVoiceRecordEnd',
                        'playVoice',
                        'onVoicePlayEnd',
                        'pauseVoice',
                        'stopVoice',
                        'uploadVoice',
                        'downloadVoice',
                        'chooseImage',
                        'previewImage',
                        'uploadImage',
                        'downloadImage',
                        'getNetworkType',
                        'openLocation',
                        'getLocation',
                        'hideOptionMenu',
                        'showOptionMenu',
                        'closeWindow',
                        'scanQRCode',
                        'chooseWXPay',
                        'openProductSpecificView',
                        'addCard',
                        'chooseCard',
                        'openCard'
                    ]
        });

    }

    fenxiang(res) {
        wx.ready(function () {
            // 分享好友
            wx.updateAppMessageShareData({
                title: res.title, // 分享标题
                desc: res.desc, // 分享描述
                link: res.link, // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
                imgUrl: res.imgUrl, // 分享图标
                success: function () {}
            })
            // 分享朋友圈
            wx.updateTimelineShareData({
                title: res.title, // 分享标题
                link: res.link, // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
                imgUrl: res.imgUrl, // 分享图标
                success: function () {}
            })
        })
    }
    // 选择照片
    chooseImage(count) {
        return new Promise((resolve) => {
            wx.chooseImage({
                count: count || 1, // 默认9
                success: function (res) {
                    var localIds = res.localIds; // 返回选定照片的本地ID列表，localId可以作为img标签的src属性显示图片
                    resolve(localIds)
                }
            })
        })
    }
    // 上传图片
    uploadImage(localId) {
        return new Promise((resolve) => {
            wx.uploadImage({
                localId: localId, // 需要上传的图片的本地ID，由chooseImage接口获得
                isShowProgressTips: 0, // 默认为1，显示进度提示
                success: function (res) {
                    var serverId = res.serverId; // 返回图片的服务器端ID
                    resolve(serverId)
                }
            })
        })
    }
    // 下载图片
    downloadImage(serverId) {
        return new Promise((resolve) => {
            wx.downloadImage({
                serverId: serverId, // 需要下载的图片的服务器端ID，由uploadImage接口获得
                isShowProgressTips: 0, // 默认为1，显示进度提示
                success: function (res) {
                    var localId = res.localId; // 返回图片下载后的本地ID
                    resolve(localId)
                }
            })
        })
    }
    // 浏览图片
    previewImage(current, urls) {
        wx.previewImage({
            current: current, // 当前显示图片的http链接
            urls: urls || [] // 需要预览的图片http链接列表
        })
    }

    // 获取地理位置接口
    getLocation() {
        return new Promise((resolve) => {
            wx.getLocation({
                type: 'wgs84', // 默认为wgs84的gps坐标，如果要返回直接给openLocation用的火星坐标，可传入'gcj02'
                success: function (res) {
                    resolve(res)
                }
            })
        })
    }
    // 使用微信内置地图查看位置接口
    openLocation(res) {
        wx.openLocation({
            latitude: res.latitude, // 纬度，浮点数，范围为90 ~ -90
            longitude: res.longitude, // 经度，浮点数，范围为180 ~ -180。
            name: res.name, // 位置名
            address: res.address, // 地址详情说明
            scale: res.scale || 12, // 地图缩放级别,整形值,范围从1~28。默认为最大
            infoUrl: res.infoUrl // 在查看位置界面底部显示的超链接,可点击跳转
        })
    }
    // 扫码
    scanQRCode() {
        return new Promise((resolve) => {
            wx.scanQRCode({
                needResult: 0, // 默认为0，扫描结果由微信处理，1则直接返回扫描结果，
                scanType: ["qrCode", "barCode"], // 可以指定扫二维码还是一维码，默认二者都有
                success: function (res) {
                    var result = res.resultStr; // 当needResult 为 1 时，扫码返回的结果
                    resolve(result)
                }
            })
        })
    }

    // 共享收货地址
    openAddress() {
        return new Promise((resolve) => {
            wx.openAddress({
                success: function (res) {
                    resolve(res)
                }
            })
        })
    }
    // 授权回来的跳转的url
    wxzf(url) {
        let bdStorage = new Storage()
        if (!bdStorage.getItem('getOpenid')) {
            let urlse = 'https%3a%2f%2fduxinggj.com%2fwww%2fphone%2fxiongmaogu%23%2f' + url
            window.location.href = `https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxf8b0c920900b2663&redirect_uri=${urlse}&response_type=code&scope=snsapi_userinfo&state=STATE#wechat_redirect`
        } else {
            router.push({
                path: url
            })
        }
    }
}
export default new dxpublic().__proto__
