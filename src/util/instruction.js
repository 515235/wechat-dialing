//instruction.js 自定义指令
class instruction {
    constructor() {
        this.checkArray = this.speaking.checkArray(this)
    }
    // 权限 
    static permission = {
        inserted: function (el, binding) {
            let permission = binding.value // 获取到 v-permission的值
            if (permission) {
                let hasPermission = checkArray(permission)
                if (!hasPermission) {
                    // 没有权限 移除Dom元素
                    el.parentNode && el.parentNode.removeChild(el)
                }
            }
        },
    }
    //end
}

function checkArray(key) {
    let arr = [1, 2, 3, 4]
    let index = arr.indexOf(key)
    if (index > -1) {
        return true // 有权限
    } else {
        return false // 无权限
    }
}
export default instruction
