import Vue from 'vue'
import urls from './urls'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
   
    {
        path: '/',
        name: 'index',
        meta: {
            title: ''
        },
        component: () => import('@/views/index.vue')
  }, {
        path: '/dialing',
        name: 'dialing',
        meta: {
            title: '拨号'
        },
        component: () => import('@/views/dialing.vue')
  },{
        path: '/callWaiting',
        name: 'callWaiting',
        meta: {
            title: '呼叫等待'
        },
        component: () => import('@/views/callWaiting.vue')
  },{
        path: '/loader',
        name: 'loader',
        meta: {
            title: '登录'
        },
        component: () => import('@/views/loader.vue')
  },  {
        path: '/test',
        name: 'test',
        meta: {
            title: '路由导航'
        },
        component: () => import('@/views/test.vue')
  },  {
        path: '/showVideo',
        name: 'showVideo',
        meta: {
            title: '路由导航'
        },
        component: () => import('@/views/showVideo.vue')
  }
]

const router = new VueRouter({
    routes,
    urls
})

export default router
