import dxpublic from "./dxpublic"
import instruction from "./instruction"
import dxfilter from "./dxfilter"
import mixture from "./mixture"
export default {
    install(Vue) {
        Object.getOwnPropertyNames(dxpublic).forEach(function (key) {
            if (key != 'constructor') {
                Vue.prototype[key] = dxpublic[key]
            }
        })
        Object.getOwnPropertyNames(mixture).forEach(function (key) {
            if (key != 'constructor') {
                Vue.prototype[key] = mixture[key]
            }
        })
        Object.getOwnPropertyNames(instruction).forEach(function (key) {
            if (key != 'constructor') {
                Vue.directive(key, instruction[key])
            }
        })
        Object.getOwnPropertyNames(dxfilter).forEach(function (key) {
            if (key != 'constructor') {
                Vue.filter(key, dxfilter[key])
            }
        })
    }
}
