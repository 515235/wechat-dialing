class dxfilter {
    time_c(t) {
        let time = new Date()
        time.setTime(t)
        let Year = time.getFullYear(),
            Month = time.getMonth() + 1,
            Data = time.getDate() < 10 ? 0 + '' + time.getDate() : time.getDate()
        Month < 10 ? Month = 0 + '' + Month : ''
        return Year + "-" + Month + "-" + Data
    }
}
export default new dxfilter().__proto__