const routes = [
   
    {
        path: '/',
        name: 'index',
        meta: {
            title: ''
        },
        component: () => import('@/views/index.vue')
  }, {
        path: '/dialing',
        name: 'dialing',
        meta: {
            title: '拨号'
        },
        component: () => import('@/views/dialing.vue')
  }, {
        path: '/loader',
        name: 'loader',
        meta: {
            title: '登录'
        },
        component: () => import('@/views/loader.vue')
  },  {
        path: '/test',
        name: 'test',
        meta: {
            title: '路由导航'
        },
        component: () => import('@/views/test.vue')
  }
]

export default routes
