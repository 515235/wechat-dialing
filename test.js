[{
    "title": "全价票",
    "subordinate": [{
        "name": "出行人1姓名",
        "value": "乔平",
        "placeholder": "请与证件上的名字一致"
    }, {
        "name": "身份证",
        "isDropDown": true,
        "value": "73461519840510850X ",
        "placeholder": "请填写出行人1的证件号"
    }, {
        "name": "出行人2姓名",
        "value": "乔平2",
        "placeholder": "请与证件上的名字一致"
    }, {
        "name": "身份证",
        "isDropDown": true,
        "value": "73461519840510850X ",
        "placeholder": "请填写出行人2的证件号"
    }]
}, {
    "title": "半价票",
    "subordinate": [{
        "name": "出行人1姓名",
        "value": "乔平3",
        "placeholder": "请与证件上的名字一致"
    }, {
        "name": "身份证",
        "isDropDown": true,
        "value": "73461519840510850X",
        "placeholder": "请填写出行人1的证件号"
    }]
}]
