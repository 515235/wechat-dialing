import {
    Storage
} from '@/util/storage.js'
var wx = require('weixin-js-sdk');
// 操作表格数据
let wxFun = {
    data: function () {
        return {
            bdStorage: ''
        }
    },
    methods: {
        async getOpenid() {
            this.bdStorage = new Storage()
            let ress = '',
                opind = ''
            if (!this.bdStorage.getItem('getOpenid')) {
                var sd_der = window.location.href
                let code = sd_der.split('?')[1].split("code=")[1].split("&")[0];
                let res = {}
                res.code = code
                ress = await this.dxget('gzh/getOpenid', res)
                opind = ress.openid
            } else {
                opind = this.bdStorage.getItem('getOpenid')
            }
            this.bdStorage.setItem({
                name: "getOpenid",
                expires: 1000 * 60 * 60 * 2, // 过期时间为 4秒
                value: opind
            })
        },
        async zhifu(url) {
            console.log(66);
            this.bdStorage = new Storage()
            let cs = {},
                th = this
            cs.openid = this.bdStorage.getItem('getOpenid')
            cs.jiner = 1 * 100
            cs.title = '测试支付'
            let wx_cd = await this.dxget('gzh/zhifu', cs)
            if (!cs.openid) {
                this.wxzf(url)
                return
            }
            wx.chooseWXPay({
                timestamp: wx_cd.timeStamp, // 支付签名时间戳，注意微信jssdk中的所有使用timestamp字段均为小写。但最新版的支付后台生成签名使用的timeStamp字段名需大写其中的S字符
                nonceStr: wx_cd.nonceStr, // 支付签名随机串，不长于 32 位
                package: wx_cd.package, // 统一支付接口返回的prepay_id参数值，提交格式如：prepay_id=\*\*\*）
                signType: wx_cd.signType, // 签名方式，默认为'SHA1'，使用新版支付需传入'MD5'
                paySign: wx_cd.paySign, // 支付签名
                success: function () {
                    th.hf('completePayment')
                }
            });
        }
    }
}
export default {
    wxFun
}
