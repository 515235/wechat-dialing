### 效果展示

> 扫码体验demo

![](http://www.duxinggj.com/public/getqr?text=https://www.duxinggj.com/www/phone/peer/index.html#/)

> 拨号中的效果

<img src="https://duxinggj-1251133427.cos.ap-guangzhou.myqcloud.com/peer/b1.jpg" width="200px">
<img src="https://duxinggj-1251133427.cos.ap-guangzhou.myqcloud.com/peer/b2.jpg" width="300px">


> 通话中的效果

<img src="https://duxinggj-1251133427.cos.ap-guangzhou.myqcloud.com/peer/t1.jpg" width="200px">
<img src="https://duxinggj-1251133427.cos.ap-guangzhou.myqcloud.com/peer/t2.jpg" width="300px">